"use strict";

class ControlsSpec {
    /**
     * @type {Array<string>}
     */
    global_controls;

    /**
     * @type {Array<string>}
     */
    player_controls;

    /**
     * @tyoe {number}
     */
    num_players;

    /**
     * @param {Array<string>} global_controls
     * @param {Array<string>} player_controls
     * @param {number} num_players
     */
    constructor(global_controls = [], player_controls = [], num_players = 1) {
        this.global_controls = global_controls;
        this.player_controls = player_controls;
        this.num_players = num_players;
    }
}

/**
 * @property {(ui: Smolpxl2Ui) => {}} init
 */

class Smolpxl2Ui {
    /**
     * The supplied element inside which we create all our other elements.
     * @type {HTMLElement}
     */
    root;

    /**
     * @type {number} screen width in pixels.
     */
    width;

    /**
     * @type {number} screen height in pixels.
     */
    height;

    /**
     * Contains the other elements, and is a fixed size, proportional to the
     * chosen screen size of the game.
     * @type {HTMLDivElement}
     */
    container;

    /**
     * Canvas on which the graphics are drawn.
     * @type {HTMLCanvasElement}
     */
    canvas;

    /**
     * Semi-transparent div that sits in front of the canvas when visible.
     * @type {HTMLDivElement}
     */
    dimmer;

    /**
     * Contains HTML for labels, buttons etc.
     * @type {HTMLDivElement}
     */
    menu;

    /**
     * The most recent menu title that we were sent, or null if we were sent
     * title page info via set_title instead of set_menu.
     * @type {string | null}
     */
    menu_title = null;

    /**
     * The most recent selected menu item that we were sent, or null if we were
     * sent title page info via set_title instead of set_menu. @type {number |
     * null}
     */
    menu_selected = 0;

    /**
     * The most recent menu items that we were sent via set_menu.
     * @type {Array<string>}
     */
    menu_items = [];

    /**
     * The most recent title items that we were sent via set_title.
     * @type {Array<Array<string>>}
     */
    title_items = [];

    /**
     * Callback to render to the canvas (outside of the start_frames loop).
     *
     * @type {(context: CanvasRenderingContext2D, width: number, height: number) => void}
     */
    render = () => {};

    /**
     * Callback for the next frame, called repeatedly after start_frames is
     * called.
     *
     * @type {(ts: DOMHighResTimeStamp, context: CanvasRenderingContext2D, width: number, height: number) => void}
     */
    frame = () => {};

    /**
     * Are we currently in a requestAnimationFrame loop?
     *
     * @type {boolean}
     */
    frames_running = false;

    /**
     * Have we already requested a resize? If so, no need to schedule another.
     *
     * @type {boolean}
     */
    resizing = false;

    /**
     * Which touch controls are available.
     *
     * @type {ControlsSpec}
     */
    controls_spec = new ControlsSpec();

    /**
     * How to display the controls.
     * @type {"none" | "minimized" | "overlay" | "below" | "beside"}
     */
    controls_mode = "none";

    /**
     * If provided, the div for the touch controls
     * @type {Array<HTMLElement>}
     */
    controls = [];

    /**
     * If provided, the div for the smolpxl bar at the top.
     * @type {HTMLDivElement | null}
     */
    smolpxl_bar = null;

    /**
     * Create a Smolpxl2Ui inside the supplied element. Deletes any existing
     * content in root_element.
     *
     * @param {HTMLElement} root_element
     */
    constructor(root_element) {
        this.root = root_element;
        this.root.innerText = "";

        add_style_tag();

        window.addEventListener("resize", () => this.on_resize());

        // Note: ideally, we'd listen for key events more specifically on our
        // components, but this more reliably gives us events even when focus
        // is unpredictable.
        window.addEventListener("keydown", (e) => this.on_keydown(e));
        window.addEventListener("keyup", (e) => this.on_keyup(e));

        if (!this.root.classList.contains("root")) {
            this.root.classList.add("root");
        }

        // @ts-ignore (this is an HTMLDivElement)
        this.container = create_element(this.root, "div", ["container"]);
        this.container.style.aspectRatio = "1";

        // @ts-ignore (this is an HTMLCanvasElement)
        this.canvas = create_element(this.container, "canvas", ["canvas"]);

        // @ts-ignore (this is an HTMLDivElement)
        this.dimmer = create_element(this.container, "div", ["dimmer"]);
        // @ts-ignore (this is an HTMLDivElement)
        this.menu = create_element(this.container, "div", ["menu"]);

        // Default screen size - probably immediately overridden
        this.set_screen_size(20, 20);
    }

    /**
     * Called when the screen resizes. You should not need to call this
     * manually.
     */
    on_resize() {
        if (this.resizing) {
            return;
        }
        this.resizing = true;
        // The screen has resized - we must allow the layout to happen
        // before we fix the sizes of things, so make our container
        // small and invisible.
        this.container.style.width = "0px";
        this.container.style.height = "0px";
        this.container.style.display = "none";

        // In the next cycle, when the browser's layout is done, size
        // our components correctly.
        setTimeout(() => {
            this.resizing = false;
            if (this.controls.length === 0) {
                this.controls = create_controls(
                    this,
                    this.root,
                    this.controls_spec,
                    this.controls_mode,
                );
            }
            this.refresh();
        });
    }

    /**
     * @param {KeyboardEvent} e
     */
    on_keydown(e) {
        this.send_event(keyboard_event_to_evt(e, "key_down"));
    }

    /**
     * @param {KeyboardEvent} e
     */
    on_keyup(e) {
        this.send_event(keyboard_event_to_evt(e, "key_up"));
    }

    /**
     * Redraw all screen elements. You should not need to call this manually -
     * it is called after the screen resizes.
     */
    refresh() {
        this.container.style.display = "grid";
        // Re-draw based on all the stuff we have remembered the
        // engine asked us to do.
        // If we end up storing too much stuff to be able to
        // do this, we could alternatively call some kind of
        // refresh method on engine instead.
        this.set_screen_size(this.width, this.height);
        if (this.menu_title !== null) {
            this.set_menu(this.menu_title, this.menu_selected, this.menu_items);
        } else {
            this.set_title(this.title_items);
        }
        this.request_render();
    }

    /**
     * Set the resolution of the canvas.
     * @param {number} width (in pixels) of the screen
     * @param {number} height (in pixels) of the screen
     */
    set_screen_size(width, height) {
        this.width = width;
        this.height = height;
        this.canvas.width = width;
        this.canvas.height = height;

        const smolpxl_bar_height = this.smolpxl_bar
            ? this.smolpxl_bar.clientHeight
            : 0;
        const controls_height = calc_controls_height(
            this.controls,
            this.controls_mode,
        );
        const root_w = this.root.clientWidth;
        const root_h =
            this.root.clientHeight - (smolpxl_bar_height + controls_height);
        const root_ratio = root_w / root_h;
        const container_ratio = width / height;
        if (root_ratio > container_ratio) {
            // Our root div is wider than us - need gaps at the sides
            this.container.style.width = `${root_h * container_ratio}px`;
            this.container.style.height = `${root_h}px`;
        } else {
            // Our root div is narrower than us - need gaps at the top/bottom
            this.container.style.width = `${root_w}px`;
            this.container.style.height = `${root_w / container_ratio}px`;
        }

        this.container.style.aspectRatio = container_ratio.toPrecision(4);
    }

    /**
     * Show the bar of buttons at the top of the screen.
     */
    show_smolpxl_bar() {
        // TODO: smolpxl bar at top, outside the container.
        if (!this.smolpxl_bar) {
            this.smolpxl_bar = create_smolpxl_bar(this.root);
        }
        this.on_resize();
    }

    /**
     * Set up which touch controls are available.
     * @param {Array<string>} global_controls a list of buttons to make
     * available, not associated with a specific player. Allowed strings:
     * Anything that is a valid value for the key property of a KeybardEvent
     * (https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key)
     * plus any of the following strings:
     * ArrowKeys, ButtonA, ButtonB, ButtonC, ButtonX, ButtonY, ButtonZ,
     * ButtonLB, ButtonLT, ButtonLSB, ButtonRB, ButtonRT, ButtonRSB,
     * ButtonBack, ButtonStart
     * @param {Array<string>} player_controls a list of buttons that each player
     * needs. Note:if different players need different buttons, these should be
     * set via global_controls.
     * @param {number} num_players how many copies of the player buttons we
     * should display.
     */
    set_controls(global_controls, player_controls, num_players) {
        this.controls_spec = new ControlsSpec(
            global_controls,
            player_controls,
            num_players,
        );
        this.controls.forEach((c) => c.remove());
        this.controls = [];
        this.on_resize();
    }

    /**
     * Change how the controls are displayed.
     * * "none" - no controls are shown.
     * * "minimized" - only a small "show controls" button is visible.
     * * "overlay" - the controls are displayed over the top of the game canvas.
     * * "below" - the controls are displayed in a separate panel below canvas.
     * * "beside" - the controls are displayed in a separate panel beside.
     *
     * @param {"none" | "minimized" | "overlay" | "below" | "beside"} mode
     */
    set_controls_mode(mode) {
        this.controls_mode = mode;
        this.controls.forEach((c) => c.remove());
        this.controls = [];
        this.on_resize();
    }

    /**
     * Supply a function to be called whenever we redraw the canvas.
     * @param {(context: CanvasRenderingContext2D, width: number, height: number) => void} render_fn
     */
    set_render(render_fn) {
        this.render = render_fn;
    }

    /**
     * Supply a function to be called on every animation frame.
     * @param { (ts: DOMHighResTimeStamp, context: CanvasRenderingContext2D, width: number, height: number) => void }
     * frame_fn
     */
    set_frame(frame_fn) {
        this.frame = frame_fn;
    }

    /**
     * Supply a function to be called when an event happens, like the user
     * chooses a menu option, or clicks a UI button.
     *
     * The function should take a single argument that is an object with a
     * `name` property. Depending on what type of event it represents, it may
     * have other properties too.
     *
     * @typedef {{ "name": "show_controls_clicked" }} ShowControlsClicked
     * @typedef {{
     *     name: "player_button_down",
     *     player: number,
     *     button: string,
     *     remote?: string,
     * }} PlayerButtonDown
     * @typedef {{
     *     name: "player_button_up",
     *     player: number,
     *     button: string,
     *     remote?: string,
     * }} PlayerButtonUp
     * @typedef {{
     *     name: "general_button_down",
     *     button: string,
     *     remote?: string,
     * }} GeneralButtonDown
     * @typedef {{
     *     name: "general_button_up",
     *     button: string,
     *     remote?: string,
     * }} GeneralButtonUp
     * @typedef {{
     *     name: "key_down",
     *     key: string,
     * }} KeyDown
     * @typedef {{
     *     name: "key_up",
     *     key: string,
     * }} KeyUp
     *
     * @typedef {ShowControlsClicked
     *     | PlayerButtonDown
     *     | PlayerButtonUp
     *     | GeneralButtonDown
     *     | GeneralButtonUp
     *     | KeyDown
     *     | KeyUp
     * } Evt
     *
     * @param {(ev: Evt) => void} event_handler
     */
    set_event_handler(event_handler) {
        this.event_handler = event_handler;
    }

    /**
     * @param {Evt} ev
     */
    send_event(ev) {
        if (this.event_handler) {
            this.event_handler(ev);
        }
    }

    /**
     * @returns {CanvasRenderingContext2D}
     */
    canvas_context() {
        /** @type {CanvasRenderingContext2D} */
        // @ts-ignore (this is a CanvasRenderingContext2D)
        const ctx = this.canvas.getContext("2d", {
            alpha: false,
            antialias: false,
        });
        ctx.imageSmoothingEnabled = false;
        return ctx;
    }

    /**
     * Manually ask for the canvas to be redrawn.
     */
    request_render() {
        if (this.render) {
            this.render(
                this.canvas_context(),
                this.canvas.width,
                this.canvas.height,
            );
        } else {
            throw new Error(
                "You must call set_render before calling request_render!",
            );
        }
    }

    /**
     * Set up a requestAnimationFrame loop and call frame()
     * every time.
     */
    start_frames() {
        if (!this.frame) {
            throw new Error(
                "You must call set_frame before calling start_frames!",
            );
        }
        this.frames_running = true;
        const ctx = this.canvas_context();
        const width = this.canvas.width;
        const height = this.canvas.height;
        requestAnimationFrame((ts) => this.on_raf(ts, ctx, width, height));
    }

    /**
     * Stop the requestAnimationFrame loop.
     */
    stop_frames() {
        this.frames_running = false;
    }

    /**
     * @param {DOMHighResTimeStamp} ts
     * @param {CanvasRenderingContext2D} canvasContext
     * @param {number} width the width of the canvas
     * @param {number} height the height of the canvas
     */
    on_raf(ts, canvasContext, width, height) {
        if (this.frames_running) {
            this.frame(ts, canvasContext, width, height);
            requestAnimationFrame((new_ts) =>
                this.on_raf(new_ts, canvasContext, width, height),
            );
        }
    }

    /**
     * Show or update the menu.
     * If items is empty, remove the menu and don't dim the game.
     * If items is non-empty, dim the canvas and display the items and title.
     * @param {string} title
     * @param {number} selected the index of the selected menu item
     * @param {Array<string>} items
     */
    set_menu(title, selected, items) {
        this.menu_title = title;
        this.menu_selected = selected;
        this.menu_items = items;
        this.title_items = [];
        if (items.length === 0) {
            this.dimmer.style.visibility = "hidden";
            this.menu.innerText = "";
        } else {
            const fontSizeGuesser = new FontSizeGuesser(
                this.container.clientWidth,
                this.container.clientHeight,
                items.length + 3,
            );

            this.dimmer.style.visibility = "visible";
            this.menu.style.visibility = "visible";

            const existingElementFinder = new ExistingElementFinder(this.menu);
            const firstExisting = existingElementFinder.next();
            updateMenuElement(this.menu, firstExisting, "", "div");

            const secondExisting = existingElementFinder.next();
            const secondElement = updateMenuElement(
                this.menu,
                secondExisting,
                title,
                "div",
            );
            secondElement.classList.add("menu-title");
            secondElement.style.fontSize = fontSizeGuesser.fontSize(
                title.length,
                0.8,
            );

            items.forEach((item, index) => {
                const existingElement = existingElementFinder.next();
                /** @type {HTMLAnchorElement} */
                // @ts-ignore this is an HTMLAnchorElement
                const item_element = updateMenuElement(
                    this.menu,
                    existingElement,
                    item,
                    "a",
                );
                item_element.style.fontSize = fontSizeGuesser.fontSize(
                    item.length,
                );
                item_element.href = "#";
                if (
                    index === selected &&
                    document.activeElement !== item_element
                ) {
                    item_element.focus();
                }
            });

            const lastExisting = existingElementFinder.next();
            updateMenuElement(this.menu, lastExisting, "", "div");

            let el;
            while ((el = existingElementFinder.next())) {
                el.remove();
                existingElementFinder.onRemoved();
            }
        }
    }

    /**
     * Show a dialog and get an answer from the user.
     *
     * @param {string} message
     * @param {string} defaultValue
     * @returns {string}
     */
    prompt(message, defaultValue) {
        return prompt(message, defaultValue);
    }

    /**
     * Provide items to be drawn within the grid on top of the canvas.
     * If items is empty, remove the menu and don't dim the game.
     * If items is non-empty, dim the canvas and display the items.
     * @param {Array<Array<string>>} items
     */
    set_title(items) {
        this.menu_title = null;
        this.menu_selected = null;
        this.menu_items = [];
        this.title_items = items;
        if (items.length === 0) {
            this.dimmer.style.visibility = "hidden";
            this.menu.innerText = "";
        } else {
            const fontSizeGuesser = new FontSizeGuesser(
                this.container.clientWidth,
                this.container.clientHeight,
                items.length,
            );

            this.dimmer.style.visibility = "visible";
            this.menu.innerText = "";
            this.menu.style.visibility = "visible";
            for (const row of items) {
                for (const item of row) {
                    const item_element = create_element(this.menu, "div", [
                        "menuitem",
                    ]);
                    item_element.style.fontSize = fontSizeGuesser.fontSize(
                        item.length,
                    );
                    item_element.innerText = item;
                }
            }
        }
    }
}

/**
 * Update the provided element to match the supplied item, or create a new one
 * if needed.
 *
 * @param {Element} parent
 * @param {Element} existingElement
 * @param {string} item
 * @param {string} tag
 * @returns {HTMLElement}
 */
function updateMenuElement(parent, existingElement, item, tag) {
    if (existingElement) {
        if (existingElement.tagName === tag.toUpperCase()) {
            // @ts-ignore this is an HTMLElement
            updateExistingMenuElement(existingElement, item);
            // @ts-ignore this is an HTMLElement
            return existingElement;
        } else {
            const newElement = newMenuElement(parent, item, tag);
            parent.insertBefore(newElement, existingElement);
            parent.removeChild(existingElement);
            return newElement;
        }
    } else {
        const newElement = newMenuElement(parent, item, tag);
        parent.appendChild(newElement);
        return newElement;
    }
}

/**
 * @param {HTMLElement} element
 * @param {string} item
 */
function updateExistingMenuElement(element, item) {
    element.innerText = item;
}

/**
 * @param {Element} parent
 * @param {string} item
 * @param {string} tag
 * @returns {HTMLAnchorElement}
 */
function newMenuElement(parent, item, tag) {
    /** @type {HTMLAnchorElement} */
    // @ts-ignore this is an HTMLAnchorElement
    const element = create_element(parent, tag, ["menuitem"]);
    element.innerText = item;
    return element;
}

class ExistingElementFinder {
    /**
     * @type {HTMLCollection}
     */
    children;

    /**
     * @type {number}
     */
    currentIndex;

    /**
     * @param {Element} parent
     */
    constructor(parent) {
        this.children = parent.children;
        this.currentIndex = 0;
    }

    /**
     * @returns {Element | null} the next child of the parent
     */
    next() {
        // item returns null if the index is out of bounds
        const ret = this.children.item(this.currentIndex);
        this.currentIndex++;
        return ret;
    }

    onRemoved() {
        this.currentIndex--;
    }
}

class FontSizeGuesser {
    /**
     * @type {number}
     */
    containerWidth;

    /**
     * @type {number}
     */
    fontHeight;

    /**
     * @param {number} containerWidth
     * @param {number} containerHeight
     * @param {number} numItems
     */
    constructor(containerWidth, containerHeight, numItems) {
        this.containerWidth = containerWidth;
        this.fontHeight = (0.5 * containerHeight) / numItems;
    }

    /**
     * @param {number} textLength
     * @param {number} multiplier scale supplied font size by this much
     *
     * @returns {string} CSS property value for the font size.
     */
    fontSize(textLength, multiplier = 1.0) {
        const guessed_width = textLength * this.fontHeight * 0.6;
        let adjusted_font_height =
            guessed_width > this.containerWidth
                ? this.containerWidth / (textLength * 0.6)
                : this.fontHeight;
        adjusted_font_height *= multiplier;

        return `${adjusted_font_height.toPrecision(2)}px`;
    }
}

/**
 * Create a new DOM element with the supplied properties.
 * @param {HTMLElement} parent
 * @param {string} tag
 * @param {Array<string>} [classes]
 * @param {boolean} [at_start]
 * @returns {HTMLElement}
 */
function create_element(parent, tag, classes, at_start) {
    const element = document.createElement(tag);
    if (classes && classes.length > 0) {
        element.classList.add(...classes);
    }
    if (at_start) {
        parent.insertBefore(element, parent.firstChild);
    } else {
        parent.appendChild(element);
    }
    return element;
}

/**
 * Creates the bar at the top of the screen.
 * @param {HTMLElement} root the containing div
 * @returns {HTMLDivElement}
 */
function create_smolpxl_bar(root) {
    /**
     * @param {HTMLDivElement} bar
     * @param {string} svg the icon as SVG in text form
     * @param {string} text the label to go next to the icon
     */
    function create_back_button(bar, svg, text) {
        /** @type {HTMLAnchorElement} */
        // @ts-ignore (this in an HTMLAnchorElement)
        const button = create_element(bar, "a", ["left"]);
        button.href = "https://smolpxl.artificialworlds.net/";

        /** @type {HTMLSpanElement} */
        // @ts-ignore (this in an HTMLSpanElement)
        const back_span = create_element(button, "span");
        back_span.innerHTML = svg;
        const back_text_span = create_element(back_span, "span");
        back_text_span.innerText = text;
    }

    /**
     * @param {HTMLDivElement} bar
     * @param {string} svg the icon as SVG in text form
     * @param {string} text the label to go next to the icon
     */
    function create_button(bar, svg, text) {
        /** @type {HTMLAnchorElement} */
        // @ts-ignore (this in an HTMLAnchorElement)
        const button = create_element(bar, "a");
        button.href = "#";

        /** @type {HTMLSpanElement} */
        // @ts-ignore (this in an HTMLSpanElement)
        const back_span = create_element(button, "span");
        back_span.innerHTML = svg;
        const back_text_span = create_element(back_span, "span", [], true);
        back_text_span.innerText = text;
    }

    const back_svg =
        '<svg aria-label="left" class="leftimg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>';
    const like_svg =
        '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>';
    const share_svg =
        '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="18" cy="5" r="3"></circle><circle cx="6" cy="12" r="3"></circle><circle cx="18" cy="19" r="3"></circle><line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line><line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line></svg>';
    const remote_svg =
        '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>';
    const code_svg =
        '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14.7 6.3a1 1 0 0 0 0 1.4l1.6 1.6a1 1 0 0 0 1.4 0l3.77-3.77a6 6 0 0 1-7.94 7.94l-6.91 6.91a2.12 2.12 0 0 1-3-3l6.91-6.91a6 6 0 0 1 7.94-7.94l-3.76 3.76z"></path></svg>';

    /** @type {HTMLDivElement} */
    // @ts-ignore (this is an HTMLDivElement)
    const bar = create_element(root, "div", ["smolpxl_bar"], true);
    create_back_button(bar, back_svg, "Smolpxl");
    create_button(bar, like_svg, "Like");
    create_button(bar, share_svg, "Share");
    create_button(bar, remote_svg, "Remote");
    create_button(bar, code_svg, "Code");

    return bar;
}

/**
 * @param {Array<HTMLElement>} controls
 * @param {"none" | "minimized" | "overlay" | "below" | "beside"} controls_mode
 */
function calc_controls_height(controls, controls_mode) {
    if (controls.length === 0 || controls_mode === "overlay") {
        return 0;
    } else {
        return controls[0].clientHeight;
    }
}

/**
 * @param {Smolpxl2Ui} ui
 * @param {HTMLElement} root
 * @param {ControlsSpec} controls
 * @param {"none" | "minimized" | "overlay" | "below" | "beside"} controls_mode
 * @returns {Array<HTMLElement>}
 */
function create_controls(ui, root, controls, controls_mode) {
    switch (controls_mode) {
        case "minimized":
            return create_controls_minimized(ui, root);
        case "overlay":
            return create_controls_overlay(ui, root, controls);
        case "below":
            // TODO: other controls
            //return create_controls_below(ui, root, controls);
            return [];
        case "beside":
            //return create_controls_beside(ui, root, controls);
            return [];
        default:
            return [];
    }
}

/**
 * @param {Smolpxl2Ui} ui
 * @param {HTMLElement} root
 * @returns {Array<HTMLElement>}
 */
function create_controls_minimized(ui, root) {
    const ret = document.createElement("a");
    ret.innerText = "show controls 🎮";
    ret.classList.add("smolpxl_controls");
    ret.classList.add("smolpxl_controls_minimized");
    ret.href = "#";
    ret.addEventListener("click", () =>
        ui.send_event({ name: "show_controls_clicked" }),
    );
    root.append(ret);
    return [ret];
}

/**
 * @param {Smolpxl2Ui} ui
 * @param {HTMLElement} root
 * @param {ControlsSpec} controls
 * @returns {Array<HTMLElement>}
 */
function create_controls_overlay(ui, root, controls) {
    const global = document.createElement("div");
    global.classList.add("smolpxl_controls_overlay_global");
    global.classList.add("smolpxl_controls_overlay");
    for (const ctrl of controls.global_controls) {
        const button = document.createElement("button");
        button.innerText = ctrl;
        button.addEventListener("pointerdown", () =>
            ui.send_event({
                name: "general_button_down",
                button: ctrl,
            }),
        );
        button.addEventListener("pointerup", () =>
            ui.send_event({
                name: "general_button_up",
                button: ctrl,
            }),
        );
        global.appendChild(button);
    }

    const players = [];
    for (let player = 0; player < controls.num_players; ++player) {
        const player_div = document.createElement("div");
        player_div.classList.add("smolpxl_controls_overlay_player");
        player_div.classList.add("smolpxl_controls_overlay");
        for (const ctrl of controls.player_controls) {
            player_div.appendChild(create_buttons(ui, ctrl, player));
        }
        players.push(player_div);
    }

    const portrait = false;
    const flow1 = document.createElement("div");
    const flow2 = document.createElement("div");
    flow1.classList.add("smolpxl_controls");
    flow2.classList.add("smolpxl_controls");
    root.appendChild(flow1);
    root.appendChild(flow2);

    if (portrait) {
        flow1.classList.add("smolpxl_controls_overlay_panel_top");
        flow2.classList.add("smolpxl_controls_overlay_panel_bottom");
    } else {
        flow1.classList.add("smolpxl_controls_overlay_panel_left");
        flow2.classList.add("smolpxl_controls_overlay_panel_right");
    }

    if (players.length === 0) {
        flow2.appendChild(global);
    } else {
        let f1 = true;
        for (const item of [global, ...players]) {
            if (f1) {
                flow1.appendChild(item);
            } else {
                flow2.appendChild(item);
            }
            f1 = !f1;
        }
    }

    return [flow1, flow2];
}

/**
 * Create a button or group of buttons for the named control. E.g. if name is
 * "ButtonA" return a single button but if it is "ArrowKeys" return a div
 * containing 4 buttons for the directions.
 *
 * @param {Smolpxl2Ui} ui
 * @param {string} name
 * @param {number} player
 * @returns {HTMLElement}
 */
function create_buttons(ui, name, player) {
    if (name === "ArrowKeys") {
        const div = document.createElement("div");
        const left = create_buttons(ui, "ArrowLeft", player);
        const right = create_buttons(ui, "ArrowRight", player);
        const up = create_buttons(ui, "ArrowUp", player);
        const down = create_buttons(ui, "ArrowDown", player);
        div.classList.add("smolpxl_arrowkeys");
        left.classList.add("smolpxl_arrowkeys_left");
        right.classList.add("smolpxl_arrowkeys_right");
        up.classList.add("smolpxl_arrowkeys_up");
        down.classList.add("smolpxl_arrowkeys_down");
        div.appendChild(left);
        div.appendChild(right);
        div.appendChild(up);
        div.appendChild(down);
        return div;
    } else {
        const button = document.createElement("button");
        button.innerHTML = button_icon(name);
        button.addEventListener("pointerdown", () =>
            ui.send_event({
                name: "player_button_down",
                player,
                button: name,
            }),
        );
        button.addEventListener("pointerup", () =>
            ui.send_event({
                name: "player_button_up",
                player,
                button: name,
            }),
        );
        return button;
    }
}

const svg_button_left =
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left-circle"><circle cx="12" cy="12" r="10"></circle><polyline points="12 8 8 12 12 16"></polyline><line x1="16" y1="12" x2="8" y2="12"></line></svg>';

const svg_button_right =
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right-circle"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg>';

const svg_button_up =
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-circle"><circle cx="12" cy="12" r="10"></circle><polyline points="16 12 12 8 8 12"></polyline><line x1="12" y1="16" x2="12" y2="8"></line></svg>';

const svg_button_down =
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down-circle"><circle cx="12" cy="12" r="10"></circle><polyline points="8 12 12 16 16 12"></polyline><line x1="12" y1="8" x2="12" y2="16"></line></svg>';

/**
 * Given the key to go on a button (e.g. "ArrowLeft") return the HTML of a
 * button for it (e.g. an SVG of a left arrow).
 *
 * @param {string} key
 * @returns {string} HTML for the button icon
 */
function button_icon(key) {
    switch (key) {
        case "ArrowLeft":
            return svg_button_left;
        case "ArrowRight":
            return svg_button_right;
        case "ArrowUp":
            return svg_button_up;
        case "ArrowDown":
            return svg_button_down;
        default: {
            if (key.slice(0, 6) === "Button") {
                return key.slice(6);
            } else {
                return key;
            }
        }
    }
}

/**
 * @param {Smolpxl2Ui} _ui
 * @param {HTMLElement} _root
 * @param {ControlsSpec} _controls
 * @returns {Array<HTMLElement>}
 */
/*function create_controls_below(_ui, _root, _controls) {
    return [];
}*/

/**
 * @param {Smolpxl2Ui} _ui
 * @param {HTMLElement} _root
 * @param {ControlsSpec} _controls
 * @returns {Array<HTMLElement>}
 */
/*function create_controls_beside(_ui, _root, _controls) {
    return [];
}*/

/**
 * @param {KeyboardEvent} e
 * @param {"key_down" | "key_up"} name
 * @returns {Evt}
 */
function keyboard_event_to_evt(e, name) {
    return {
        name,
        key: e.key,
    };
}

/**
 * Add a style tag to the document that provides CSS to control appearance and
 * layout of the smolpxl2-ui DOM elements.
 *
 * If another instance has already created this tag, do nothing.
 */
function add_style_tag() {
    if (document.getElementById("smolpxl2_ui_style")) {
        // Some other copy of smolpxl2-ui has already added our styles
        return;
    }

    const style = document.createElement("style");
    style.innerHTML = `
        .root {
            background-color: rgb(240, 240, 240);
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            font-family: monospace;
            font-variant: small-caps;
            font-weight: bold;
            font-size: 14px;
            color: white;
            position: relative;
        }

        .smolpxl_bar {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding-top: 2px;
            padding-bottom: 2px;
            width: 100%;
        }

        .smolpxl_bar a {
            background-color: #eeeeee;
            color: black;
            display: inline-block;
            font-size: 96%;
            text-decoration: none;
            padding: 2px 6px;
            border-radius: 6px;
            border: 2px solid #2f4858;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, .3);
            margin-right: 4px;
        }

        .smolpxl_bar a span {
          display: flex;
          align-items: center;
        }

        .smolpxl_bar a svg {
            height: 1em;
            width: 1em;
            margin-left: 0.3em;
        }

        .smolpxl_bar a.left svg {
            margin-left: 0em;
            margin-right: 0.1em;
        }

        .smolpxl_controls button {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .smolpxl_controls_minimized {
            align-self: end;
            padding-right: 0.3em;
            padding-bottom: 0.1em;
            text-decoration: none;
            color: black;
        }

        .smolpxl_controls_overlay {
            opacity: 0.7;
        }

        .smolpxl_controls_overlay_panel_left {
            left: 0.5em;
            align-items: flex-start;
            z-index: 3;
        }

        .smolpxl_controls_overlay_panel_right {
            right: 0.5em;
            align-items: flex-end;
            z-index: 3;
        }

        .smolpxl_controls_overlay_panel_left,.smolpxl_controls_overlay_panel_right {
            position: absolute;
            top: 3em;
            height: calc(100% - 4em);
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .smolpxl_controls_overlay_panel_left > div,.smolpxl_controls_overlay_panel_right > div {
            display: flex;
            flex-direction: column;
            gap: 0.3em;
        }

        .smolpxl_controls_overlay_panel_left button,.smolpxl_controls_overlay_panel_right button {
            font-size: 1.5em;
            min-width: 2em;
            min-height: 2em
        }

        .smolpxl_controls_overlay_panel_left > div {
            align-items: center;
        }

        .smolpxl_controls_overlay_panel_right > div {
            align-items: center;
        }

        .smolpxl_arrowkeys {
            display: grid;
            grid-template-rows: 1.5em 1.5em 1.5em 1.5em;
            gap: 0.3em;
        }

        .smolpxl_arrowkeys_left {
            grid-column: 1;
            grid-row: 2;
        }

        .smolpxl_arrowkeys_right {
            grid-column: 3;
            grid-row: 2;
        }

        .smolpxl_arrowkeys_up {
            grid-column: 2;
            grid-row: 1 / 2;
        }

        .smolpxl_arrowkeys_down {
            grid-column: 2;
            grid-row: 3 / 4;
        }

        .container {
            display: grid;
            justify-items: stretch;
            align-items: stretch;
            overflow: auto;
        }

        .canvas {
            z-index: 0;
            grid-row: 1;
            grid-column: 1;
            image-rendering: pixelated;
        }

        .dimmer {
            z-index: 1;
            background-color: black;
            grid-row: 1;
            grid-column: 1;
            opacity: 0.7;
            visibility: hidden;
        }

        .menu {
            z-index: 2;
            grid-row: 1;
            grid-column: 1;
            display: grid;
            align-items: center;
            justify-items: center;
            visibility: hidden;
            margin-top: 15%;
            margin-bottom: 15%;
        }

        .menu a {
            color: white;
            text-decoration: none;
            padding: 0.1em 0.2em;
            margin: -0.1em -0.2em;
        }

        .menu a:focus {
            outline: 0.15em solid white;
        }

        .menu-title {
            color: #CCC;
        }
    `;
    style.id = "smolpxl2_ui_style";
    document.head.appendChild(style);
}

export { Smolpxl2Ui };
