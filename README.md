# smolpxl2-ui

Smolpxl2 is a game engine for creating retro-style pixellated games in a
browser.

This project is a thin JavaScript layer that is designed to be driven by an
engine such as smolpxl2-engine-rs.

It provides a canvas that scales to fit the containing element, a grid-based UI
that sits on top, and a small top navigation bar, and touch player controls.

## Example:

To see an example of how it looks with no proper engine hooked up, run:

```bash
python3 -m http.server
```

in this directory, and then visit http://0.0.0.0:8000/

[index.html](index.html) also serves as an example of how to use it smolpxl2-ui,
although you will probably want to use a full-featured engine like
smolpxl2-engine-rs - that project has examples of how to use it.

## License and credits

Copyright 2023 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Contains icons from the
[Feather Icons](https://github.com/feathericons/feather) set, which is
Copyright 2013-2017 Cole Bemis, and released under the
[MIT License](https://github.com/feathericons/feather/blob/8b5d6802fa8fd1eb3924b465ff718d2fa8d61efe/LICENSE).

Uses [shareon](https://shareon.js.org/) by Nikita Karamov to provide the
social sharing buttons.  (The code is dynamically loaded when the Share button
is clicked.)

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)

