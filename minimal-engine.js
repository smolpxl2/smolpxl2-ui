"use strict";

// eslint-disable-next-line no-unused-vars
import { Smolpxl2Ui } from "./smolpxl2-ui.js";

class MinimalEngineTitle {
    /**
     * @param {Smolpxl2Ui} ui
     */
    constructor(ui) {
        ui.set_screen_size(20, 10);

        ui.set_title([
            ["Demo game 1"],
            ["with"],
            ["a menu visible here! and some extra long text"],
        ]);

        ui.set_render((c) => render_something(c, 20, 10));
    }
}

class MinimalEngineButtons {
    /**
     * @param {Smolpxl2Ui} ui
     */
    constructor(ui) {
        this.ui = ui;

        this.ui.set_screen_size(20, 20);

        this.ui.show_smolpxl_bar();

        this.ui.set_controls(["Escape", "Enter"], ["ArrowKeys", "ButtonA"], 3);

        this.ui.set_controls_mode("overlay");

        this.ui.set_render((c, width, height) =>
            render_something(c, width, height),
        );
        this.ui.set_event_handler((ev) => this.handle_event(ev));
    }

    /**
     * @param {{name: string}} ev the event that happened. See
     *                             Smolpxl2Ui.set_event_handler for the
     *                             allowed types.
     */
    handle_event(ev) {
        switch (ev.name) {
            case "show_controls_clicked":
                return this.ui.set_controls_mode("overlay");
            case "player_button_down":
            case "player_button_up":
            case "key_down":
            case "key_up":
                return;
            default: {
                console.error(`Unknown event ${ev.name}`);
                return;
            }
        }
    }
}

class MinimalEnginePlain {
    /**
     * @param {Smolpxl2Ui} ui
     */
    constructor(ui) {
        ui.set_screen_size(10, 10);
        ui.set_frame((ts, ctx, width, height) =>
            render_noise(ts, ctx, width, height),
        );
        ui.start_frames();
    }
}

class MinimalEngineMenu {
    /**
     * @param {Smolpxl2Ui} ui
     */
    constructor(ui) {
        ui.set_screen_size(20, 10);

        ui.set_menu("My Menu", 1, [
            "Start",
            "Players",
            "Something very long and complex",
            "Remote",
        ]);

        ui.set_render((c) => render_something(c, 20, 10));
    }
}

/**
 * @param {number} _ts
 * @param {CanvasRenderingContext2D} canvasContext
 * @param {number} width
 * @param {number} height
 */
function render_noise(_ts, canvasContext, width, height) {
    for (let x = 0; x < width; x++) {
        for (let y = 0; y < height; y++) {
            const g = Math.floor(Math.random() * 256);
            canvasContext.fillStyle = `rgb(0, ${g}, 0)`;
            canvasContext.fillRect(x, y, 1, 1);
        }
    }
}

/**
 * @param {CanvasRenderingContext2D} canvasContext
 * @param {number} width
 * @param {number} height
 */
function render_something(canvasContext, width, height) {
    canvasContext.fillStyle = "rgb(0, 0, 0)";
    canvasContext.fillRect(0, 0, width, height);
    canvasContext.fillStyle = "rgb(255, 0, 0)";
    canvasContext.fillRect(2, 2, 1, 1);
    canvasContext.fillStyle = "rgb(0, 255, 0)";
    canvasContext.fillRect(3, 3, 1, 1);
}

export {
    MinimalEngineTitle,
    MinimalEngineButtons,
    MinimalEnginePlain,
    MinimalEngineMenu,
};
