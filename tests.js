"use strict";

// eslint-disable-next-line no-unused-vars
import { Smolpxl2Ui } from "./smolpxl2-ui.js";

// @ts-ignore mocha and chai are brought in via index.html
const it = window.it;
// @ts-ignore
const expect = chai.expect; // eslint-disable-line no-undef

class EmptyEngine {
    /** @type {Smolpxl2Ui} */
    smolpxl2ui;

    /**
     * @param {Smolpxl2Ui} smolpxl2ui
     */
    constructor(smolpxl2ui) {
        this.smolpxl2ui = smolpxl2ui;
    }
}

/**
 * Create a div with a grid layout and add it to the document.
 *
 * @param {string} id the ID of the created div
 * @param {number} w1 the width of the first column in pixels
 * @param {number} h1 the height of the first row in pixels
 * @param {number} w2 the width of the second column in pixels
 * @param {number} h2 the height of the second row in pixels
 * @returns {HTMLDivElement} the created div
 */
function create_grid(id, w1, h1, w2, h2) {
    const div = document.createElement("div");
    div.id = id;
    div.style.display = "grid";
    div.style.alignContent = "stretch";
    div.style.alignItems = "stretch";
    div.style.width = `${w1 + w2}px`;
    div.style.height = `${h1 + h2}px`;
    div.style.gridTemplateColumns = `${w1}px ${w2}px`;
    div.style.gridTemplateRows = `${h1}px ${h2}px`;
    document.body.appendChild(div);
    return div;
}

/**
 * Create a div and make a smolpxl instance. on it
 * @param {HTMLDivElement} surround a div (with grid display) that will contain
 *                         the div we are creating
 * @param {number} grid_x the 1-based column in the grid to add the div.
 * @param {number} grid_y the 1-based row in the grid to add the div.
 * @param {number} canvas_width_px the width in pixels of the canvas
 * @param {number} canvas_height_px the height in pixels of the canvas
 * @param {boolean} [add_smolpxl_bar] show the smolpxl bar if true
 * @param {"none" | "minimized" | "overlay" | "below" | "beside"} [add_controls]
 *        show controls if true
 *
 * @returns {{
 *     root: HTMLDivElement,
 *     canvas: HTMLCanvasElement,
 *     dimmer: HTMLDivElement,
 *     menu: HTMLDivElement,
 *     smolpxl2ui: Smolpxl2Ui,
 *     smolpxl_bar: HTMLDivElement | undefined,
 *     controls: HTMLElement | undefined,
 * }} the created div (root) and canvas (canvas)
 */
function create_smolpxl(
    surround,
    grid_x,
    grid_y,
    canvas_width_px,
    canvas_height_px,
    add_smolpxl_bar,
    add_controls = "none",
) {
    /** @type {HTMLDivElement} */
    const root = document.createElement("div");
    const smolpxl2ui = new Smolpxl2Ui(root);
    new EmptyEngine(smolpxl2ui);
    root.style.gridRow = `${grid_y}`;
    root.style.gridColumn = `${grid_x}`;
    surround.appendChild(root);
    smolpxl2ui.set_screen_size(canvas_width_px, canvas_height_px);
    if (add_smolpxl_bar) {
        smolpxl2ui.show_smolpxl_bar();
    }
    smolpxl2ui.set_controls([], [], 1);
    smolpxl2ui.set_controls_mode(add_controls);

    return {
        root,
        canvas: root.querySelector("canvas"),
        dimmer: root.querySelector(".dimmer"),
        menu: root.querySelector(".menu"),
        smolpxl2ui,
        smolpxl_bar: root.querySelector(".smolpxl_bar"),
        controls: root.querySelector(".smolpxl_controls"),
    };
}

it("fits the root to the parent container", /** @param {() => void} done */ (done) => {
    // Given a grid layout with specified row/col size
    const surround = create_grid("fits-root-to-parent", 45, 41, 55, 59);

    // When I add smolpxl instances to the top-left and bottom-right
    const { root: root1 } = create_smolpxl(surround, 1, 1, 20, 20);
    const { root: root2 } = create_smolpxl(surround, 2, 2, 20, 20);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then they take their size from the container
        expect(root1.clientWidth).to.equal(45);
        expect(root1.clientHeight).to.equal(41);
        expect(root2.clientWidth).to.equal(55);
        expect(root2.clientHeight).to.equal(59);
        done();
    });
});

it("creates a canvas scaled to fit root", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("canvas-scaled", 140, 40, 60, 160);

    // When I add square smolpxl instances to the top-left and bottom-right
    const { canvas: canvas1 } = create_smolpxl(surround, 1, 1, 20, 20);
    const { canvas: canvas2 } = create_smolpxl(surround, 2, 2, 20, 20);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvases are scaled to fit the grid squares
        expect(canvas1.clientWidth).to.equal(40);
        expect(canvas1.clientHeight).to.equal(40);
        expect(canvas2.clientWidth).to.equal(60);
        expect(canvas2.clientHeight).to.equal(60);
        done();
    });
});

it("aspect ratio of canvas matches its pixel shape", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("aspect-ratio-canvas", 140, 40, 60, 160);

    // When I add square smolpxl instances to the top-left and bottom-right
    const { canvas: canvas1 } = create_smolpxl(surround, 1, 1, 60, 30);
    const { canvas: canvas2 } = create_smolpxl(surround, 2, 2, 10, 20);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvases are scaled to fit the grid squares
        expect(canvas1.clientWidth).to.equal(80);
        expect(canvas1.clientHeight).to.equal(40);
        expect(canvas2.clientWidth).to.equal(60);
        expect(canvas2.clientHeight).to.equal(120);
        done();
    });
});

it("creates a canvas scaled to fit root (with smolpxl bar, landscape)", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("canvas-scaled", 540, 440, 0, 0);

    // When I add square smolpxl instances to the top-left and bottom-right
    const { canvas: canvas1, smolpxl_bar: smolpxl_bar1 } = create_smolpxl(
        surround,
        1,
        1,
        20,
        20,
        true,
    );

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvas is scaled to fit the grid squares
        expect(smolpxl_bar1.clientHeight).to.be.greaterThan(10);
        expect(canvas1.clientWidth).to.equal(440 - smolpxl_bar1.clientHeight);
        expect(canvas1.clientHeight).to.equal(440 - smolpxl_bar1.clientHeight);
        done();
    });
});

it("creates a canvas scaled to fit root (with smolpxl bar, portrait)", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("canvas-scaled", 440, 540, 0, 0);

    // When I add square smolpxl instances to the top-left and bottom-right
    const { canvas: canvas1, smolpxl_bar: smolpxl_bar1 } = create_smolpxl(
        surround,
        1,
        1,
        20,
        20,
        true,
    );

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvas is scaled to fit the grid squares
        expect(smolpxl_bar1.clientHeight).to.be.greaterThan(10);
        expect(canvas1.clientWidth).to.equal(440);
        expect(canvas1.clientHeight).to.equal(440);
        done();
    });
});

it("creates a canvas scaled to fit root (minimized controls, landscape)", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("canvas-scaled", 540, 440, 0, 0);

    // When I add square smolpxl instances to the top-left and bottom-right
    const {
        root,
        canvas: canvas1,
        smolpxl_bar: smolpxl_bar1,
    } = create_smolpxl(surround, 1, 1, 20, 20, true, "minimized");

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvas is scaled to fit the grid squares
        const controls = root.querySelector(".smolpxl_controls");
        const extra_height = smolpxl_bar1.clientHeight + controls.clientHeight;
        expect(smolpxl_bar1.clientHeight).to.be.greaterThan(10);
        expect(controls.clientHeight).to.be.greaterThan(10);
        expect(canvas1.clientWidth).to.equal(440 - extra_height);
        expect(canvas1.clientHeight).to.equal(440 - extra_height);
        done();
    });
});

it("creates a canvas scaled to fit root (minimized controls, portrait)", /** @param {() => void} done */ (done) => {
    // Given a grid layout with a wide cell and a tall cell
    const surround = create_grid("canvas-scaled", 440, 540, 0, 0);

    // When I add square smolpxl instances to the top-left and bottom-right
    const { canvas: canvas1, smolpxl_bar: smolpxl_bar1 } = create_smolpxl(
        surround,
        1,
        1,
        20,
        20,
        true,
        "minimized",
    );

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the canvas is scaled to fit the grid squares
        expect(smolpxl_bar1.clientHeight).to.be.greaterThan(10);
        expect(canvas1.clientWidth).to.equal(440);
        expect(canvas1.clientHeight).to.equal(440);
        done();
    });
});

it("fits title screen items vertically into area", /** @param {() => void} done */ (done) => {
    // When I create a short menu
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { canvas, menu, smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);
    smolpxl2ui.set_title([["A"], ["B"]]);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the text fits on the screen
        // /** @type Array<HTMLDivElement> */
        const items = menu.querySelectorAll(".menuitem");
        const menu_rect = menu.getBoundingClientRect();
        const canvas_rect = canvas.getBoundingClientRect();
        const item0_rect = items[0].getBoundingClientRect();
        const item1_rect = items[1].getBoundingClientRect();

        expect(item0_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item1_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item0_rect.width).to.be.lessThan(100);
        expect(item1_rect.width).to.be.lessThan(100);
        expect(item0_rect.height).to.be.lessThan(40);
        expect(item1_rect.height).to.be.lessThan(40);

        expect(item0_rect.y - canvas_rect.y).to.be.lessThan(50);
        expect(item1_rect.y - canvas_rect.y).to.be.greaterThan(50);

        expect(window.getComputedStyle(items[0]).fontSize).to.equal("25px");
        expect(window.getComputedStyle(items[1]).fontSize).to.equal("25px");

        // And the menu fits the canvas
        expect(menu_rect.x).to.be.greaterThanOrEqual(canvas_rect.x);
        expect(menu_rect.width).to.be.lessThanOrEqual(canvas_rect.width);
        expect(menu_rect.y).to.be.greaterThanOrEqual(canvas_rect.y);
        expect(menu_rect.height).to.be.lessThanOrEqual(canvas_rect.height);

        done();
    });
});

it("fits long title screen items horizontally by shrinking", /** @param {() => void} done */ (done) => {
    // When I create a short menu
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { canvas, menu, smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);
    smolpxl2ui.set_title([["long long long long"], ["B"]]);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the long item fits on the screen
        // /** @type Array<HTMLDivElement> */
        const items = menu.querySelectorAll(".menuitem");
        const menu_rect = menu.getBoundingClientRect();
        const canvas_rect = canvas.getBoundingClientRect();
        const item0_rect = items[0].getBoundingClientRect();

        expect(item0_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item0_rect.width).to.be.lessThan(100);
        expect(item0_rect.height).to.be.lessThan(40);

        expect(item0_rect.y - canvas_rect.y).to.be.lessThan(50);

        expect(window.getComputedStyle(items[0]).fontSize).to.equal("8.8px");

        // And the menu fits the canvas
        expect(menu_rect.x).to.be.greaterThanOrEqual(canvas_rect.x);
        expect(menu_rect.width).to.be.lessThanOrEqual(canvas_rect.width);
        expect(menu_rect.y).to.be.greaterThanOrEqual(canvas_rect.y);
        expect(menu_rect.height).to.be.lessThanOrEqual(canvas_rect.height);

        done();
    });
});

it("draws the menu using anchor tags", /** @param {() => void} done */ (done) => {
    // When I create a short menu
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { canvas, menu, smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);
    smolpxl2ui.set_menu("my menu", 1, ["A", "B"]);

    setTimeout(() => {
        // (wait for layout to happen)
        // Then the text fits on the screen
        // /** @type Array<HTMLElement> */
        const items = menu.querySelectorAll(".menuitem");
        const menu_rect = menu.getBoundingClientRect();
        const canvas_rect = canvas.getBoundingClientRect();
        const item1_rect = items[1].getBoundingClientRect();
        const item2_rect = items[2].getBoundingClientRect();
        const item3_rect = items[3].getBoundingClientRect();

        expect(items[0].tagName).to.equal("DIV");
        expect(items[1].tagName).to.equal("DIV");
        expect(items[2].tagName).to.equal("A");
        expect(items[3].tagName).to.equal("A");
        expect(items[4].tagName).to.equal("DIV");
        expect(items[0].textContent).to.equal("");
        expect(items[1].textContent).to.equal("my menu");
        expect(items[2].textContent).to.equal("A");
        expect(items[3].textContent).to.equal("B");
        expect(items[4].textContent).to.equal("");

        expect(item1_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item2_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item3_rect.x - canvas_rect.x).to.be.lessThan(50);
        expect(item1_rect.width).to.be.lessThan(100);
        expect(item2_rect.width).to.be.lessThan(100);
        expect(item1_rect.width).to.be.lessThan(100);
        expect(item2_rect.height).to.be.lessThan(40);
        expect(item3_rect.height).to.be.lessThan(40);

        expect(item1_rect.y - canvas_rect.y).to.be.lessThan(50);
        expect(item2_rect.y - canvas_rect.y).to.be.lessThan(50);
        expect(item3_rect.y - canvas_rect.y).to.be.greaterThan(50);

        expect(window.getComputedStyle(items[1]).fontSize).to.equal("8px");
        expect(window.getComputedStyle(items[2]).fontSize).to.equal("10px");
        expect(window.getComputedStyle(items[3]).fontSize).to.equal("10px");

        // And the menu fits the canvas
        expect(menu_rect.x).to.be.greaterThanOrEqual(canvas_rect.x);
        expect(menu_rect.width).to.be.lessThanOrEqual(canvas_rect.width);
        expect(menu_rect.y).to.be.greaterThanOrEqual(canvas_rect.y);
        expect(menu_rect.height).to.be.lessThanOrEqual(canvas_rect.height);

        done();
    });
});

it("removes extra items when drawing a shorter menu", /** @param {() => void} done */ (done) => {
    // When I create a long menu
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { canvas, menu, smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);
    smolpxl2ui.set_menu("my menu", 1, ["A", "B", "C", "D"]);

    setTimeout(() => {
        // (wait for layout to happen)

        // Sanity
        const items = menu.querySelectorAll(".menuitem");
        expect(items.length).to.equal(7);

        // And then create a shorter one
        smolpxl2ui.set_menu("my menu", 1, ["Removes extra items"]);
        setTimeout(() => {
            const items = menu.querySelectorAll(".menuitem");
            expect(items.length).to.equal(4);
        });

        done();
    });
});

it("requests to draw to canvas when prompted", () => {
    // Given a UI and engine
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);

    /** @type {null | CanvasRenderingContext2D} */
    let f_called_with = null;
    const f = /** @param {CanvasRenderingContext2D} ctx */ (ctx) => {
        f_called_with = ctx;
    };

    // When I request a callback
    smolpxl2ui.set_render(f);
    smolpxl2ui.request_render();

    // Then it gets called with a rendercontext
    expect(f_called_with).to.be.a("CanvasRenderingContext2D");
});

it("requests to draw multiple times when switched on", /** @param {() => void} done */ function (done) {
    this.slow(200);

    // Given a UI and engine
    const surround = create_grid("menu-list", 100, 100, 0, 0);
    const { smolpxl2ui } = create_smolpxl(surround, 1, 1, 20, 20);

    /** @type {Array<Array<any>>} */
    const f_called_with = [];
    const f = /** @param {Array<any>} args */ (...args) => {
        if (f_called_with.length < 5) {
            f_called_with.push(args);
        }
    };

    // When I request to start frames
    smolpxl2ui.set_frame(f);
    smolpxl2ui.start_frames();

    // Then I get update and render calls
    /** @type {() => boolean} */
    const frame_called_3_times = () => {
        return (
            f_called_with.length > 2 &&
            f_called_with[0].length === 4 &&
            typeof f_called_with[0][0] == "number" &&
            typeof f_called_with[0][1] == "object" &&
            typeof f_called_with[0][2] == "number" &&
            typeof f_called_with[0][3] == "number"
        );
    };

    /** @type {number} */
    let num_tries = 0;

    const keep_trying = /** @param {() => boolean} checks_pass */ (
        checks_pass,
    ) => {
        if (num_tries > 100) {
            smolpxl2ui.stop_frames();
            throw new Error("Checks failed!");
        }
        if (checks_pass()) {
            smolpxl2ui.stop_frames();
            done();
        } else {
            num_tries++;
            setTimeout(() => keep_trying(checks_pass), 10);
        }
    };

    keep_trying(frame_called_3_times);
});
