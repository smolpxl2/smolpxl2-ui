all: lint

lint:
	npx prettier --check .
	npx eslint .
	npx --package typescript tsc --project jsconfig.json

format:
	npx prettier --write .
